<?php
    require_once "./vendor/autoload.php";
    ini_set('display_errors', 1);
    $config = array_merge([
        "host" => null,
        "port" => null,
        "encryption" => null,
        "email" => null,
        "password" => null
    ], file_exists("./config.php") ? require_once "./config.php" : []);


    function getParam($request, $name, $default = null){
        if (!empty($request)){
            foreach ($request as $key => $value){
                $request[mb_strtolower($key)] = $value;
            }
        }
        $name = mb_strtolower($name);
        return isset($request[$name]) ? $request[$name] : $default;
    }

    function checkIP(){
        $ip = $_SERVER["REMOTE_ADDR"];
        if (file_exists(FILE_NAME) && is_file(FILE_NAME)){
            $json = json_decode(file_get_contents(FILE_NAME), true);
            if (isset($json[$ip])){
                if ($json[$ip] + MAX_TIME > time()){
                    return false;
                }
            }

        }
        return true;
    }

    function setIP(){
        $json = [];
        $ip = $_SERVER["REMOTE_ADDR"];
        if (file_exists(FILE_NAME) && is_file(FILE_NAME)){
            $json = json_decode(file_get_contents(FILE_NAME), true);
        }
        $json[$ip] = time();
        file_put_contents(FILE_NAME, json_encode($json));
    }

    function sendMail($items, $name, $phone, $address, $config = []){
        if (
            count(
                array_filter(
                    $config,
                    function($value){
                        return empty($value);
                    }
                )
            ) == 0
        ){
            try{
                $transport = (new Swift_SmtpTransport($config["host"], $config["port"]))
                    ->setUsername($config["email"])
                    ->setPassword($config["password"])
                    ->setEncryption($config["encryption"])
                ;
                $mailer = new Swift_Mailer($transport);
                $text = [date("d.m.Y") . " в " . date("H:i") . " поступила заявку на покупку."];
                $text[] = "Клиент: " . addslashes($name) . " / " . addslashes($phone);
                $text[] = "Адресс: " . addslashes($address);
                $text[] = "Позиции:";
                foreach ($items as $name => $total){
                    $text[] = addslashes($name) . " | " . intval($total);
                }
                $text = implode(PHP_EOL, $text);
                $message =  (new Swift_Message("Заявка на доставку"))
                    ->setTo($config["email"])
                    ->setFrom($config["email"])
                    ->setBody($text);
                $mailer->send($message);
            } catch (\Exception $e){

            }
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: OPTIONS, PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-Http-Request");

        define('FILE_NAME', __DIR__ . DIRECTORY_SEPARATOR . "block.json");
        define('MAX_TIME', 600);

        $response = [
            "status" => 0,
            "message" => "Неизвестная ошибка"
        ];
        $name = getParam($_POST, 'name');
        $phone = preg_replace('/[^\d]/', '', getParam($_POST, 'phone'));
        $address = getParam($_POST, 'address');
        $count = array_filter(
            getParam($_POST, 'count', []),
            function($value){
                return intval($value);
            }
        );
        if (count($count) == 0){
            $response["message"] = "Вы не указали кол-во товара";
        }elseif (empty($name)){
            $response["message"] = "Укажите Ваше имя";
        }elseif (empty($phone)){
            $response["message"] = "Укажите Ваш телефон";
        }elseif (strlen($phone) < 7){
            $response["message"] = "Телефон должен быть более 7 цифр";
        }elseif (empty($address)){
            $response["message"] = "Укажите адрес доставки";
        }else{
            if (!checkIP()){
                $response["message"] = "Вы не можете так часто отправлять запрос";
            }else{
                $response["status"] = 1;
                $response["message"] = 'Наш менеджер свяжется с Вами в ближайшее время';
                setIP();
                sendMail($count, $name, $phone, $address, $config);
            }
        }
        $ajax = getParam(getallheaders(), "X-Http-Request", false);
        if ($ajax){
            header('Content-Type: application/json');
            echo json_encode($response);
        }else{
            header('Location: /');
        }
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Вешалки для химчисток оптом</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.3/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="apple-touch-icon" sizes="57x57" href="./assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="./assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="./assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
    <header id="section-header">
        <div class="container">
            <div class="logotype">
                <a href="/">
                    <img src="/assets/image/logotype.png" alt="Логотип">
                </a>
            </div>
            <nav class="navigation">
                <ul>
                    <li><a href="#section-about">О нас</a></li>
                    <li><a href="#section-delivery">Доставка</a></li>
                    <li><a href="#section-map">Контакты</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <div id="section-jumbotron">
        <div class="container">
            <h1>Вешалки для химчисток и прачечных оптом</h1>
            <div class="triggers">
                <div class="trigger"><span class="icon"><i class="fa fa-home"></i></span><span
                        class="text">Производим</span></div>
                <div class="trigger"><span class="icon"><i class="fa fa-hand-o-right"></i></span><span
                        class="text">Продаем</span></div>
                <div class="trigger"><span class="icon"><i class="fa fa-truck"></i></span><span
                        class="text">Доставляем</span></div>
            </div>
        </div>
    </div>
    <div class="block-container" id="section-shop">
        <div class="container">
            <h2 class="block-title">Вешалки из качественной проволоки</h2>
            <div class="block-shop-item">
                <div class="image">
                    <img src="/assets/image/item1.jpg" alt="Вешалка малого диаметра 2.2мм">
                </div>
                <div class="info">
                    <div class="params">
                        2,2 мм
                    </div>
                    <div class="name">Вешалка малого диаметра 2.2мм</div>
                    <div class="count"><i class="fa fa-dot-circle-o"></i>500 шт. в коробке</div>
                    <div class="info">
                        Вешалка для прачечных и химчисток, для легких вещей и трикотажа. Не ломается, отлично держит
                        форму. С выемками.
                    </div>
                    <div class="point"><i class="fa fa-map-marker"></i> На складе СПб. Доставка</div>
                    <a href="#price-table" class="more" data-id="1">Подробнее</a>
                </div>
            </div>
            <div class="block-shop-item">
                <div class="image">
                    <img src="/assets/image/item2.jpg" alt="Вешалка большого диаметра 2.7мм">
                </div>
                <div class="info">
                    <div class="params">
                        2,7 мм
                    </div>
                    <div class="name">Вешалка большого диаметра 2.7мм</div>
                    <div class="count"><i class="fa fa-dot-circle-o"></i>400 шт. в коробке</div>
                    <div class="info">
                        Толстая вешалка для более тяжелых вещей идеальна для северных регионов. Выдержит все, от плаща
                        до пальто. Усиленная форма.
                    </div>
                    <div class="point"><i class="fa fa-map-marker"></i>На складе СПб. Доставка</div>
                    <a href="#price-table" class="more" data-id="2"">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
    <div id="section-params">
        <div class="container">
            <div class="items">
                <div class="item">
                    <div class="value"><span data-counter="24">0</span> часа</div>
                    <div class="text">в сутки работает производство</div>
                </div>
                <div class="item">
                    <div class="value"><span  data-counter="2500">0</span>+</div>
                    <div class="text">Коробок производится ежемесячно</div>
                </div>
                <div class="item">
                    <div class="value"><span data-counter="1125000">0</span>+</div>
                    <div class="text">Вешалок производится ежемесячно</div>
                </div>
                <div class="item">
                    <div class="value"><span data-counter="25">0</span>+</div>
                    <div class="text">Химчисток стали нашими клиентами</div>
                </div>
            </div>
        </div>
    </div>
    <div id="section-why" class="block-container">
        <div class="container">
            <h2 class="block-title">Почему наши вешалки?</h2>
            <div class="block-shop-item">
                <div class="image">
                    <img src="/assets/image/why.png" alt="Почему наши вешалки?">
                </div>
                <div class="info">
                    <div class="item">
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <div class="description">
                            <div class="title">Гладкость</div>
                            <div class="text">Гладкая оцинкованная поверхность</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon"><i class="fa fa-bookmark"></i></div>
                        <div class="description">
                            <div class="title">Качество</div>
                            <div class="text">Чистая, яркая металлическая проволока</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="icon"><i class="fa fa-th"></i></div>
                        <div class="description">
                            <div class="title">Форма</div>
                            <div class="text">Признанная во всем мире форма вешалки для химчисток и прачечных</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-pricelist">
        <div class="container">
            <a href="javascript:void(null)" data-click="modal">
                <i class="fa fa-cloud-download"></i>
                Сделать заказ
            </a>
        </div>
    </div>

    <div id="section-manufacter" class="block-container">
        <div class="container">
            <h2 class="block-title">
                Наше производство
            </h2>
            <div class="block-shop-item">
                <div class="info">
                    <h3>Современные станки</h3>
                    <div class="subtitle">ВешалкиОпт</div>
                    <p>Мы предлагаем к продаже вешалки-плечики собственного производства. Наши химчистки, как и многие
                        наши партнеры столкнулись с импортозамещением. В 2019 году было принято решение об открытии
                        производства. Наши вешалки изготовлены из лучшей в стране оцинкованной проволоки завода
                        «Северсталь», отличаются своей гладкостью, блеском, достаточной упругостью и идеально чистой
                        поверхностью.</p>
                    <ul>
                        <li><i class="fa fa-dot-circle-o"></i>Большой опыт в сфере чистоты</li>
                        <li><i class="fa fa-dot-circle-o"></i>Налаженная логистика</li>
                        <li><i class="fa fa-dot-circle-o"></i>Свои производственные мощности </li>
                    </ul>
                </div>
                <div class="image">
                    <img src="/assets/image/manufacter.png" alt="Современные станки">
                </div>
            </div>
        </div>
        <div id="section-about">
            <div class="container">
                    <h2 class="block-title">О компании</h2>
                    <p>Компания ВешалкиОпт предлагает к продаже вешалки собственного производства. Имея свои химчистки, мы
                        также, как и многие столкнулись с задачей импортозамещения. Было решено открыть свое производство
                        вешалок. Наши вешалки изготовлены из лучшей в стране оцинкованной проволоки завода «Северсталь»,
                        отличаются своей гладкостью, блеском, достаточной упругостью и идеально чистой поверхностью.
                    </p>
            </div>
        </div>
        <div class="container">
            <h3 class="block-title">Прайс лист на металлические вешалки</h3>
            <div class="block-table-pricelist">
                <table id="price-table">
                    <thead>
                        <tr>
                            <th>Объем коробок в месяц</th>
                            <th>2.2мм (500 шт.)</th>
                            <th class="col col-2">2.7мм (400 шт.)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Свыше 100 коробок в ассортименте</td>
                            <td class="col col-1">договорная</td>
                            <td class="col col-2">договорная</td>
                        </tr>
                        <tr>
                            <td>от 51 до 100 коробок в ассортименте</td>
                            <td class="col col-1">1800</td>
                            <td class="col col-2">2300</td>
                        </tr>
                        <tr>
                            <td>от 11 до 50 коробок в ассортименте</td>
                            <td class="col col-1">1900</td>
                            <td class="col col-2">2400</td>
                        </tr>
                        <tr>
                            <td>от 6 до 10 коробок в ассортименте</td>
                            <td class="col col-1">2000</td>
                            <td class="col col-2">2500</td>
                        </tr>
                        <tr>
                            <td>До 5 коробок в ассортименте</td>
                            <td class="col col-1">2100</td>
                            <td class="col col-2">2600</td>
                        </tr>
                        <tr>
                            <td>Пленка ПВД</td>
                            <td colspan="2" class="col col-1 col-2">175 руб/кг</td>
                        </tr>
                        <tr>
                            <td>Картон для пиджаков (упаковка 500 шт)</td>
                            <td colspan="2" class="col col-1 col-2">2400</td>
                        </tr>
                        <tr>
                            <td>Картон для брюк (упаковка 2500 шт)</td>
                            <td colspan="2" class="col col-1 col-2">3500</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="section-delivery">
                <h3 class="block-title">Доставка и оплата</h3>
                <p>Компания ВешалкиОпт работает с большинством транспортных компаний РФ.</p>
                <p>Мы обеспечиваем доставку заказа до терминала транспортной компании в Санкт-Петербурге собственными
                    силами. Сроки доставки до терминала транспортной компании: 2-4 рабочих дня с момента поступления
                    оплаты на наш расчетный счет.
                </p>
                <p>
                    Доставка товаров по Санкт-Петербургу и области:
                    <ul>
                        <li>Доставка по Санкт-Петербургу платная, от 1 коробки.</li>
                        <li>Цена доставки включается в счет поставки.</li>
                    </ul>
                </p>
                <p>
                    Доставка заказа по Санкт-Петербургу осуществляется в течение 1-3 рабочих дней с момента поступления
                    оплаты на наш расчетный счет. Также заказ всегда можно забрать самовывозом с нашего склада.
                </p>
                <p>
                    Доставка заказа по Ленинградской области осуществляется в течение 2-4 рабочих дней с момента
                    поступления
                    оплаты на наш расчетный счет.
                </p>
                <p>
                    Доставка в отдаленные и труднодоступные регионы России обсуждается индивидуально.
                </p>
                <p>
                    <small>
                        <sup>*</sup> При перевозке по России через транспортные компании доставка до терминала
                        бесплатная.
                    </small>
                </p>
            </div>
        </div>
    </div>
    <div class="block-pricelist">
        <div class="container">
            <a href="javascript:void(null)" data-click="modal">
                <i class="fa fa-cloud-download"></i>
                Сделать заказ
            </a>
        </div>
    </div>
    <div id="section-map" data-caption="">
        <div class="map-balloon">
            <b class="title">Компания ВешалкиОпт – поставщик и производитель вешалок для химчисток</b>
            <ul>
                <li><i class="fa fa-envelope-o"></i><a href="mailto:info@veshalkiopt.ru">info@veshalkiopt.ru</a></li>
            </ul>
        </div>
    </div>
    <div id="section-footer">
        <div class="container">
            ООО "ВешалкиОпт" <?= date("Y"); ?>г. © Все права защищены
        </div>
    </div>
    <div id="modal-window">
        <div id="form-sender">
            <div class="header">
                <div class="title">Заполните заявку на заказ</div>
                <div class="close">
                    <a href="javascript:void(null);" data-click="close-modal">&times;</a>
                </div>
            </div>
            <form action="/" method="POST">
                <div class="input">
                    <input type="text" required name="name" placeholder="Ф.И.О"/>
                </div>
                <div class="input">
                    <input type="tel" required name="phone" placeholder="Ваш телефон"/>
                </div>
                <div class="input">
                    <button type="button" id="manual-delivery">Самовывоз</button><input type="text" required name="address" placeholder="Адрес доставки"/>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>Товар</th>
                            <th>Цена</th>
                            <th>Кол-во</th>
                            <th>Единица измерения</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Вешалка 2.2мм</td>
                            <td class="price">от 1800 руб</td>
                            <td>
                                <input type="text" name="count[Вешалка 2.2мм]" value="0" data-type="number" data-item="0"/>
                            </td>
                            <td class="pt">коробка (500 шт)</td>
                        </tr>
                        <tr>
                            <td>Вешалка 2.7мм</td>
                            <td class="price">от 2300 руб</td>
                            <td>
                                <input type="text" name="count[Вешалка 2.7мм]" value="0" data-type="number" data-item="1"/>
                            </td>
                            <td class="pt">коробка (400 шт)</td>
                        </tr>
                        <tr>
                            <td>Пленка ПВД</td>
                            <td class="price">175 руб</td>
                            <td>
                                <input type="text" name="count[Вешалка 2.7мм]" value="0" data-type="number" data-item="2"/>
                            </td>
                            <td class="pt">кг</td>
                        </tr>
                        <tr>
                            <td>Картон для пиджаков</td>
                            <td class="price">2400 руб</td>
                            <td>
                                <input type="text" name="count[Картон для пиджаков]" value="0" data-type="number" data-item="3"/>
                            </td>
                            <td class="pt">упаковка (500 шт)</td>
                        </tr>
                        <tr>
                            <td>Картон для брюк</td>
                            <td class="price">3500 руб</td>
                            <td>
                                <input type="text" name="count[Картон для брюк]" value="0" data-type="number" data-item="4"/>
                            </td>
                            <td class="pt">упаковка (2500 шт)</td>
                        </tr>
                    </tbody>
                    
                </table>
                <div class="summary">
                    <div class="label">Предварительная стоимость</div>
                    <div class="value" id="summary-value">0 руб</div>
                </div>
                <button type="submit">Отправить</button>
            </form>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/countup.js/2.0.4/countUp.min.js" type="module"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/countup.js/2.0.4/countUp.withPolyfill.min.js" type="module"></script>
    <script src="https://unpkg.com/imask"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.3/dist/sweetalert2.all.min.js"></script>
    <script src="/assets/js/script.js" type="module"></script>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym"); ym(56517277, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/56517277" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</body>

</html>