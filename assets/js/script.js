'use strict'

import { CountUp } from 'https://cdnjs.cloudflare.com/ajax/libs/countup.js/2.0.4/countUp.min.js';

const prices = {
    0: [
        {
            max: 5,
            price: 2100
        },
        {
            max: 10,
            price: 2000
        },
        {
            max: 50,
            price: 1900
        },
        {
            max: 100,
            price: 1800
        },
        {
            min: 101,
            price: null
        }
    ],
    1: [
        {
            max: 5,
            price: 2600
        },
        {
            max: 10,
            price: 2500
        },
        {
            max: 50,
            price: 2400
        },
        {
            max: 100,
            price: 2300
        },
        {
            min: 101,
            price: null
        }
    ],
    2: [
        {
            price: 175
        }
    ],
    3: [
        {
            price: 2400
        }
    ],
    4: [
        {
            price: 3500
        }
    ]
}

const setPriceCol = function (column) {
    let
        columns = document.querySelectorAll('#price-table .col'),
        selected = document.querySelectorAll('#price-table .col-' + column);
    if (columns.length) {
        Array.from(columns).forEach(function (element) {
            element.classList.remove('select');
        });
    }
    if (selected.length) {
        Array.from(selected).forEach(function (element) {
            element.classList.add('select');
        });
    }
}

Array.from(document.querySelectorAll('[data-click="modal"]')).forEach(function (element) {
    element.addEventListener('click', function (event) {
        event.preventDefault();
        document.body.classList.add('open-modal');
    })
});

document.querySelector('[data-click="close-modal"]').addEventListener('click', function (event) {
    event.preventDefault();
    document.body.classList.remove('open-modal');
    document.body.style.overflow = 'auto';
});


function setInputFilter(textbox, inputFilter) {
    [
        "input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"
    ].forEach(function (event) {
        textbox.addEventListener(event, function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    });
}

const inputCounter = document.querySelectorAll('[data-type="number"]');
if (inputCounter) {
    Array.from(inputCounter).forEach(function (input) {
        setInputFilter(input, function (value) {
            return /^\d*$/.test(value);
        });
    });
}
const getSummaryItems = function (itemCount) {
    let summary = 0,
        isExit = false;
    for (let i = 0, j = itemCount.length; i < j; i++) {
        if (isExit) {
            break;
        }
        let input = itemCount[i];
        if (typeof input.dataset.item != "undefined") {
            let id = input.dataset.item,
                total = parseInt(input.value);
            if (!total) {
                total = 0;
            }
            if (typeof prices[id] != "undefined") {
                for (let ix = 0, jx = prices[id].length; ix < jx; ix++) {
                    let price = prices[id][ix];
                    if (typeof price.price != "undefined") {
                        if (typeof price.min != "undefined") {
                            if (price.min <= total) {
                                if (price.price == null) {
                                    summary = null;
                                    isExit = true;
                                    break;
                                } else {
                                    summary += total * price.price;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                        if (typeof price.max != "undefined") {
                            if (price.max >= total) {
                                if (price.price == null) {
                                    summary = null;
                                    isExit = true;
                                    break;
                                } else {
                                    summary += total * price.price;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                        if (price.price == null) {
                            summary = null;
                            isExit = true;
                            break;
                        } else {
                            summary += total * price.price;
                            break;
                        }
                    }
                }
            }

        }
    }
    
    return summary;
}
const itemCount = document.querySelectorAll('#form-sender [data-type="number"]');
if (itemCount) {
    Array.from(itemCount).forEach(function (input) {
        input.addEventListener('keyup', function (event) {
            let summary = getSummaryItems(itemCount);
            summary = summary == null ? "Договорная" : summary + ' руб';
            document.getElementById('summary-value').innerHTML = summary;
        });
    });

}

const links = document.querySelectorAll('a[href="#price-table"].more');
if (links) {
    Array.from(links).forEach(function (link) {
        link.addEventListener('click', function (event) {
            const
                target = event.target,
                id = target.dataset.id;
            if (id) {
                setPriceCol(id);
            }
        });
    });
}

const Alert = function (text, title, type) {
    if (typeof Swal != "undefined") {
        type = type ? type : 'success';
        Swal.fire({
            icon: type,
            title: title,
            text: text
        })
    } else {
        alert(text);
    }
}

document.addEventListener('DOMContentLoaded', function () {
    if (typeof L == "object") {
        const mapPlace = document.getElementById('section-map');
        if (mapPlace) {
            const caption = mapPlace.innerHTML;
            mapPlace.innerHTML = '';
            const
                theTileLayer = L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
                    attribution: 'Map data &copy; 2019 OpenStreetMap contributors',
                }),
                point = L.latLng(
                    59.899803,
                    30.401010
                ),
                map = L.map(mapPlace.id).setView(point, 12).addLayer(theTileLayer);

            map.doubleClickZoom.disable();
            if (caption) {
                const marker = L.marker(point);
                marker.bindPopup(caption)
                marker.addTo(map).openPopup();
            }
        }
    }
    const phones = document.querySelectorAll('input[type="tel"]');
    if (phones.length) {
        if (typeof IMask != "undefined") {
            Array.from(phones).forEach(function (field) {
                IMask(
                    field, {
                    mask: '+{7}(000)000-00-00'
                }
                );
            });
        }
    }

    const form = document.querySelector('#form-sender form');
    if (form) {
        const md = document.getElementById('manual-delivery');
        if (md) {
            md.addEventListener('click', function (event) {
                let children = event.target.parentNode.children;
                if (children) {
                    Array.from(children).forEach(function (child) {
                        if (child.nodeName == "INPUT") {
                            child.value = "Самовывоз";
                        }
                    });
                }
            });
        }

        form.addEventListener("submit", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            event.stopPropagation();
            const
                form = event.target,
                data = new FormData(form);
            document.body.classList.add('loading');
            fetch(form.getAttribute('action'), {
                method: form.getAttribute('method'),
                mode: 'cors',
                headers: {
                    "X-Http-Request": true
                },
                body: data
            }).then(function (response) {
                document.body.classList.remove('loading');
                if (response.ok == false) {
                    Alert(response.statusText, 'Ошибка', 'error');
                } else {
                    return response.json();
                }
            }).then(function (json) {
                document.body.classList.remove('loading');
                try {
                    if (json.status == 0) {
                        Alert(json.message, 'Ошибка', 'error');
                    } else {
                        Alert(json.message, 'Сообщение', 'success');
                        document.body.classList.remove('open-modal');
                        document.body.style.overflow = 'auto';
                        form.reset();
                    }
                } catch (error) {
                    Alert(error, 'Ошибка', 'error');
                }
            }).catch(function (error) {
                document.body.classList.remove('loading');
                Alert(error, 'Ошибка', 'error');
            })
        })
    }

    window.addEventListener('scroll', function () {
        let counters = document.querySelectorAll('[data-counter]'),
            offset = pageYOffset + document.body.clientHeight;
        if (counters) {
            for (let i = 0, j = counters.length; i < j; i++) {
                let top = counters[i].offsetTop
                if (top <= offset) {
                    let counter = counters[i].dataset.counter;
                    if (!counters[i].classList.contains('visible')) {
                        counters[i].classList.add('visible');
                        if (typeof CountUp == "function") {
                            (new CountUp(counters[i], counter, {
                                duration: 1.5,
                                separator: ','
                            })).start()
                        }
                    }
                }
            }
        }
    });
});